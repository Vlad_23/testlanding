const gulp = require('gulp'),
	less = require('gulp-less'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),	
	autoprefixer = require('gulp-autoprefixer'),
	plumber = require('gulp-plumber'),
	browserSync = require('browser-sync').create();

	var devMode = false;

	gulp.task('move_img', function() {
		gulp.src('src/img/**/*')
			.pipe(imagemin({
				progressive: true,
				svgoPlugins: [{removeViewBox: false}],
				use: [pngquant()]
			}))
			.pipe(gulp.dest('./public/img/'))
			.pipe(browserSync.reload({
				stream: true
			}));
	});

	gulp.task('compile_less', function () {
		gulp.src('src/less/**/*.less')
			.pipe(plumber())
			.pipe(less())
			.pipe(autoprefixer('last 3 versions','safari 8', 'safari 7', 'safari 6', '> 2%', 'ie 9'))
			.pipe(gulp.dest('./public/css'))
			.pipe(browserSync.reload({
				stream: true
			}));
	});

	gulp.task('move_js', [], function() {
		gulp.src('./src/js/**/*.js')
			.pipe(gulp.dest('./public/js/'))
			.pipe(browserSync.reload({
				stream: true
			}));
	});

	gulp.task('move_html', [], function() {
		return gulp.src('./src/**/*.html')
			.pipe(gulp.dest('./public/'))
			.pipe(browserSync.reload({
				stream: true
			}));
	});

	gulp.task('build', function() {
		gulp.start(['move_img', 'compile_less', 'move_js', 'move_html'])
	});

	gulp.task('browser-sync', function() {
		browserSync.init(null, {
			open: false,
			server: {
				baseDir: 'public',
			}
		});
	});


	gulp.task('default', function() {
		devMode = true;
		gulp.start(['build', 'browser-sync']);
		gulp.watch('src/less/**/*.less', ['compile_less']);
		gulp.watch('src/img/**/*', ['move_img']);
		gulp.watch('src/js/**/*.js', ['move_js']);
		gulp.watch('src/**/*.html', ['move_html']);
	});
